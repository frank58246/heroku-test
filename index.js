const express = require('express')
const app = express()

var env = process.env.ASPNETCORE_ENVIRONMENT;
var message = `<h1>Hello, I'm Frank. ${env}</h1>`

app.get('/', (req, res) => res.send(message))

app.listen(process.env.PORT || 8080)